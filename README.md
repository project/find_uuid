Find UUID module for Drupal 8/9
====================================

### Find an entity having a particular UUID, within in a Drupal site.

The main use case for this module is:
- If an error message gives you just a UUID no entity ID or type, and
- You have many entity types in your site, and no easy way of finding out which entity this UUID belongs to.

Find UUID offers a simple, stateless way to ask your site "what does this UUID belong to?".

Find UUID uses the Entity API to look for all fielded entity types with a field named "uuid", searching for an entity of one of those types having the UUID you provide.

This can be done via HTTP or Drush - a controller and a command are provided.

There are two methods provided via HTTP or Drush:

1. Find all fielded types that have a UUID field:
- http://localhost/find-uuid/entities/{uuid}
- `drush find_uuid:entities {uuid}`

2. Find the entity a particular UUID belongs to (if it exists on your site).
- http://localhost/find-uuid/entity_type_ids
- `drush find_uuid:entity_types`

The drush commands support output formatting, so you may chose from any output types available on your site, for example:
- --format=csv
- -- format=tsv
- -- format=json
- etc...
- The default output type is "table"

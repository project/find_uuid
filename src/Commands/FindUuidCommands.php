<?php

namespace Drupal\find_uuid\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Console\Bootstrap\Drupal;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Formatter\OutputFormatter;

/**
 * Class FindUuidCommands.
 *
 * @package Drupal\find_uuid\Commands
 */
class FindUuidCommands extends DrushCommands {

  /**
   * Provide formatted list of entities having the provided $uuid.
   *
   * @command find_uuid:entities
   * @aliases findUuidEntities
   */
  public function findUuidEntities($uuid, $options = ['format' => 'table']) {
    $found_entities = find_uuid_entities($uuid);
    $output_array = [];
    foreach ($found_entities as $entity_type_id => $fields) {
      $output_array[] = [
        'entity_type' => $entity_type_id,
        'uuid' => $fields[0]['uuid'],
        'entity_id' => $fields[0]['id'],
      ];
    }
    return new RowsOfFields($output_array);
  }

  /**
   * Provide formatted list of fielded entity types having a uuid field.
   *
   * @command find_uuid:entity_types
   * @aliases find_uuid_entity_types
   */
  public function findEntityTypes($options = ['format' => 'table']) {
    $bundles = find_uuid_entity_types();
    $output_array = [];
    foreach ($bundles as $bundle) {
      $output_array[] = [
        'entity_type' => $bundle,
      ];
    }
    return new RowsOfFields($output_array);
  }

}

<?php

namespace Drupal\find_uuid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for Find UUID routes.
 */
class FindUuidController extends ControllerBase {

  /**
   * Provides response to search for $uuid in appropriate entities.
   *
   * @param string $uuid
   *   The uuid we are searching for.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JsonResponse w/ search results.
   */
  public function findUuidEntities(string $uuid): JsonResponse {
    $found_entities = find_uuid_entities($uuid);
    return new JsonResponse([
      'success' => count($found_entities) > 0,
      'data' => $found_entities,
      'operation' => 'findUuidEntities',
    ]);
  }

  /**
   * Provides response to request for all fielded entity types with a "uuid" field.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JsonResponse w/ search results.
   */
  public function findEntityTypeIds(): JsonResponse {
    $candidate_bundles = find_uuid_entity_types();
    return new JsonResponse([
      'success' => count($candidate_bundles) > 0,
      'data' => $candidate_bundles,
      'operation' => 'find_entity_type_ids',
    ]);
  }

}

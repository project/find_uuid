<?php

/**
 * @file
 * Primary functionality for Find UUID module.
 */

/**
 * Return an array of fieldable entity types containing a "uuid" field.
 *
 * @return array
 *   The IDs of all fielded entity types having a field named uuid.
 */
function find_uuid_entity_types(): array {
  try {
    $retval = [];
    $entity_type_defs = Drupal::entityTypeManager()->getDefinitions();
    foreach (array_keys($entity_type_defs) as $entity_type_id) {
      if (method_exists(Drupal::entityTypeManager()
        ->getDefinition($entity_type_id)
        ->getOriginalClass(), 'hasField')) {
        $field_defs = Drupal::service('entity_field.manager')
          ->getFieldDefinitions($entity_type_id, $entity_type_id);
        foreach (array_keys($field_defs) as $field_id) {

          if ($field_id === 'uuid') {
            $retval[] = $entity_type_id;
            break;
          }
        }
      }
    }
  }
  catch (Exception $e) {
    $retval = [];
    Drupal::logger('find_uuid')->error($e->getMessage());
  }
  return $retval;
}

/**
 * Return the results of an entity query looking for $uuid in $entity_type_id.
 *
 * @param string $entity_type_id
 *   The entity_type we would like search for $uuid in.
 * @param string $uuid
 *   The uuid we are searching for.
 *
 * @return array
 *   Results of an entity query looking for $uuid in $entity_type_id.
 */
function find_uuid_bundle_entities(string $entity_type_id, string $uuid): array {
  try {
    $query = Drupal::entityQuery($entity_type_id)->condition('uuid', $uuid);
    $entity_ids = $query->execute();
    $retval = $entity_ids;
  }
  catch (Exception $e) {
    $retval = [];
    Drupal::logger('find_uuid')->error($e->getMessage());
  }
  return $retval;
}

/**
 * Search for $uuid in all fielded entity types containing a "uuid" field.
 *
 * @param string $uuid
 *   The uuid we are searching for.
 *
 * @return array
 *   Results of an entity query looking for $uuid in all appropriate types.
 */
function find_uuid_entities(string $uuid): array {
  $found_entities = [];
  $candidate_bundles = find_uuid_entity_types();
  if ($candidate_bundles) {
    foreach ($candidate_bundles as $candidate_bundle) {
      $entity_ids = find_uuid_bundle_entities($candidate_bundle, $uuid);
      foreach ($entity_ids as $entity_id) {
        $found_entities[$candidate_bundle][] =
          ['uuid' => $uuid, 'id' => $entity_id];
      }
    }
  }

  return $found_entities;
}
